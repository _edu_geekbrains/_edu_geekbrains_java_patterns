package ru.syskaev.edu.geekbrains.java.patterns.lesson5;

public interface Observer {

    public void update(Object subject, SomeInfo info);

}
