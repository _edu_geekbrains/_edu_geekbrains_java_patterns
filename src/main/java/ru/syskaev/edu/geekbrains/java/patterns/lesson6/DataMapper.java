package ru.syskaev.edu.geekbrains.java.patterns.lesson6;

public interface DataMapper {

    public boolean insert(Object obj);
    public Object select(Object obj);
    //...

}
