package ru.syskaev.edu.geekbrains.java.patterns.lesson5;

import ru.syskaev.edu.geekbrains.java.patterns.lesson3.units.Unit;

public class UnitObserverConnector {

    private Unit unit;
    private UnitObserver observer;

    public UnitObserverConnector(Unit unit) {
        this.unit = unit;
    }

    public boolean attachObserver(UnitObserver observer) {
        if(this.observer != null)
            return false;
        this.observer = observer;
        return true;
    }

    public boolean detachObserver() {
        if(observer == null)
            return false;
        observer = null;
        return true;
    }

    public void updateObserver(SomeInfo info) {
        if(observer != null)
            observer.update(unit, info);
    }

}
