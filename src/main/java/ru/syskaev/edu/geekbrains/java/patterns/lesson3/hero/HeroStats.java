package ru.syskaev.edu.geekbrains.java.patterns.lesson3.hero;

import ru.syskaev.edu.geekbrains.java.patterns.lesson3.units.UnitStats;

public class HeroStats extends UnitStats {

    private String name;
    private int age;
    private char sex;
    private String alignment;
    //..

    public void setName(String name) { this.name = name; }
    public void setAge(int age) { this.age = age; }
    public void setSex(char sex) { this.sex = sex; }
    public void setAlignment(String alignment) { this.alignment = alignment; }
    //..

    public String getName() { return name; }
    public int getAge() { return age; }
    public char getSex() { return sex; }
    public String getAlignment() { return alignment; }
    //..

}
