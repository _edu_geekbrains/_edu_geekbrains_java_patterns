package ru.syskaev.edu.geekbrains.java.patterns.lesson4.singlecommands;

import ru.syskaev.edu.geekbrains.java.patterns.lesson3.hero.Hero;
import ru.syskaev.edu.geekbrains.java.patterns.lesson4.SingleCommand;

import java.util.Random;

public class AttackCommand extends SingleCommand {

    public AttackCommand(Hero hero) {
        super(hero);
        //...
    }

    @Override
    public boolean doIt() {
        System.out.println("Attack!");
        if(new Random().nextDouble() < 0.1)
            return false;
        else
            return true;

    }

}
