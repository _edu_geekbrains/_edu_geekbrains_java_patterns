package ru.syskaev.edu.geekbrains.java.patterns.lesson3.hero;

public class HeroBuilder {

    private Hero hero = new Hero();

    public HeroBuilder name(String name) {
        hero.getHeroStats().setName(name);
        return this;
    }

    public HeroBuilder age(int age) {
        hero.getHeroStats().setAge(age);
        return this;
    }

    public HeroBuilder sex(char sex) {
        hero.getHeroStats().setSex(sex);
        return this;
    }

    public HeroBuilder alignment(String alignment) {
        hero.getHeroStats().setAlignment(alignment);
        return this;
    }

    public HeroBuilder hitPoints(int hitPoints) {
        hero.getUnitStats().setHP(hitPoints);
        return this;
    }

    public HeroBuilder addToInventory(String item) {
        hero.getInventory().addItem(item);
        return this;
    }

    public Hero build() {
        return hero;
    }

}
