package ru.syskaev.edu.geekbrains.java.patterns.lesson6;

public class PseudoDB {

    private static PseudoDB instance;

    private PseudoDB() {}

    public static PseudoDB getInstance() {
        if(instance == null)
            instance = new PseudoDB();
        return instance;
    }

    public Object execQuery(String query) {
        switch (query.charAt(0)) {
            case 'I':
                return Boolean.TRUE;
            case 'S':
                return new InventoryItem("example");
            default:
                return null;
        }
    }

}
