package ru.syskaev.edu.geekbrains.java.patterns.lesson3;

public class Main {

    public static void main(String[] args) {
        Game game = Game.getInstance();
        game.startGame();
    }

}
