package ru.syskaev.edu.geekbrains.java.patterns.lesson4;

import java.util.LinkedList;
import java.util.List;

public class ComplexCommand implements Command{

    private List<Command> commands = new LinkedList<>();

    @Override
    public boolean doIt() {
        System.out.println("Complex command. Commands list size = " + commands.size());
        boolean tempBoolean = true;
        for(Command command : commands)
            tempBoolean = tempBoolean && command.doIt();
        return tempBoolean;
    }

    public void addCommand(Command command) {
        commands.add(command);
    }

}
