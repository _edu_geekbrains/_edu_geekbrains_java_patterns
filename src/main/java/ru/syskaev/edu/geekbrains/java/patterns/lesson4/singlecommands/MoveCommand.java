package ru.syskaev.edu.geekbrains.java.patterns.lesson4.singlecommands;

import ru.syskaev.edu.geekbrains.java.patterns.lesson3.hero.Hero;
import ru.syskaev.edu.geekbrains.java.patterns.lesson4.SingleCommand;

public class MoveCommand extends SingleCommand {

    private MoveCommand(Hero hero) {
        super(hero);
    }

    public MoveCommand(Hero hero, CommandType commandType) {
        this(hero);
        this.commandType = commandType;
    }

    @Override
    public boolean doIt() {
        switch(commandType) {
            case RUN:
                System.out.println("Run!");
                //..
                return true;
            case GO_VILLAGE:
                System.out.println("Go village!");
                //...
                return true;
            default:
                return false;
        }
    }
}
