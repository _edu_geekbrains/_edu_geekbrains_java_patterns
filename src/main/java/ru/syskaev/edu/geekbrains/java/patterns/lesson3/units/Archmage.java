package ru.syskaev.edu.geekbrains.java.patterns.lesson3.units;

public class Archmage extends Mage {

    public Archmage() {
        super();
        //..
    }

    @Override
    public void castAnything() {
        super.castAnything();
    }

    public void castAnithingSerious() {
        //..
    }

}
