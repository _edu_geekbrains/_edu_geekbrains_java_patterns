package ru.syskaev.edu.geekbrains.java.patterns.lesson3.units;

public enum UnitType {
    WARRIOR, ROGUE, MAGE, ARCHMAGE
}
