package ru.syskaev.edu.geekbrains.java.patterns.lesson6;

public class InventoryItemMapper implements DataMapper {

    private PseudoDB pseudoDB = PseudoDB.getInstance();

    @Override
    public boolean insert(Object item) {
        Object response = pseudoDB.execQuery("INSERT " + itemToDBEntry((InventoryItem)item));
        return response != null;
    }

    @Override
    public InventoryItem select(Object item) {
        return (InventoryItem) pseudoDB.execQuery("SELECT " + itemToDBEntry((InventoryItem)item));
    }

    private String itemToDBEntry(InventoryItem item) {
        //example
        return item.toString();
    }

}
