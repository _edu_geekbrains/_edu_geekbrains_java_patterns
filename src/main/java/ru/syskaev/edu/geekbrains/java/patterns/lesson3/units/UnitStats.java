package ru.syskaev.edu.geekbrains.java.patterns.lesson3.units;

public class UnitStats {

    private int HP;
    private int XP;
    private int AttackDamage;
    private int AttackRange;
    private double dps;

    public void setHP(int HP) { this.HP = HP; }
    public void setXP(int XP) { this.XP = XP; }
    //...

    @Override
    public String toString() {
        return "UnitStats{" +
                "HP=" + HP +
                ", XP=" + XP +
                ", AttackDamage=" + AttackDamage +
                ", AttackRange=" + AttackRange +
                ", dps=" + dps +
                '}';
    }
}
