package ru.syskaev.edu.geekbrains.java.patterns.lesson3.units;

public class UnitsFactory {

    private UnitsFactory() {}

    static public Unit createUnit(UnitType unitType) {
        switch(unitType) {
            case WARRIOR:
                return new Warrior();
            case ROGUE:
                return new Rogue();
            case MAGE:
                return new Mage();
            case ARCHMAGE:
                return new Archmage();
            default:
                return null;
        }
    }

}
