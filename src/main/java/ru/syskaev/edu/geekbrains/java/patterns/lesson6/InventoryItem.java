package ru.syskaev.edu.geekbrains.java.patterns.lesson6;

public class InventoryItem {

    enum ItemType {WEAPON, POTION, KEY, OTHER}

    private String name;
    private ItemType itemType;
    private int itemValue;
    private String description;

    private long uniqItemKey;
    public InventoryItemMapper mapper = new InventoryItemMapper();

    public InventoryItem(String name, ItemType itemType, int itemValue, String description) {
        this.name = name;
        this.itemType = itemType;
        this.itemValue = itemValue;
        this.description = description;
        uniqItemKey = genUniqItemKey();
    }

    public InventoryItem(String name) {
        this(name, ItemType.OTHER, 0, "");
    }

    public String getName() { return name; }

    private long genUniqItemKey() {
        //Some hash
        return this.hashCode() & (new Long(System.currentTimeMillis()).hashCode());
    }

    @Override
    public String toString() {
        return "InventoryItem{" +
                "name='" + name + '\'' +
                ", itemType=" + itemType +
                ", itemValue=" + itemValue +
                ", description='" + description + '\'' +
                '}';
    }

}
