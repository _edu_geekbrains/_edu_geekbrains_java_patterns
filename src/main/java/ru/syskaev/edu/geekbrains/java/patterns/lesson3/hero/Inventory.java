package ru.syskaev.edu.geekbrains.java.patterns.lesson3.hero;

import ru.syskaev.edu.geekbrains.java.patterns.lesson6.InventoryItem;

import java.util.ArrayList;
import java.util.List;

public class Inventory {

    private List<InventoryItem> inventoryList = new ArrayList<>();
    //..

    public boolean addItem(String itemName) {
        return addItem(new InventoryItem(itemName));
    }

    public boolean addItem(InventoryItem item) {
        item.mapper.insert(item);
        return inventoryList.add(item);
    }

    public InventoryItem getItem(String itemName) {
        for (InventoryItem item : inventoryList)
            if(item.getName().equals(itemName)) {
                InventoryItem itemFromDB = item.mapper.select(item);
                if(itemFromDB != null)
                    return item;
                break;
            }
        return null;
    }

    public InventoryItem getItem(InventoryItem item) {
        InventoryItem itemFromDB = item.mapper.select(item);
        int indexInList = inventoryList.indexOf(item);
        if (itemFromDB != null && indexInList != -1)
            return inventoryList.get(indexInList);
        else
            return null;
    }

}
