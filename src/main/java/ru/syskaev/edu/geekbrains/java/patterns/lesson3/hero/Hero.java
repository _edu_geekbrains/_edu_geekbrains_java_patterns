package ru.syskaev.edu.geekbrains.java.patterns.lesson3.hero;

import ru.syskaev.edu.geekbrains.java.patterns.lesson3.units.Unit;
import ru.syskaev.edu.geekbrains.java.patterns.lesson3.units.UnitStats;
import ru.syskaev.edu.geekbrains.java.patterns.lesson5.UnitObserverConnector;

import java.awt.geom.Point2D;

public class Hero implements Unit {

    private HeroStats heroStats = new HeroStats();
    private Inventory inventory = new Inventory();
    //...

    @Override
    public UnitStats getUnitStats() {
        return getHeroStats();
    }

    @Override
    public void setUnitStats() {
        //..
    }

    public HeroStats getHeroStats() {
        return heroStats;
    }

    public Inventory getInventory() { return inventory; }

    @Override
    public void attack(Unit unit) {
        //..
    }

    @Override
    public boolean goTo(Point2D targetPoint) {
        //..
        return false;
    }

    @Override
    public UnitObserverConnector getObserverConnector() {
        return null;
    }

}
