package ru.syskaev.edu.geekbrains.java.patterns.lesson5;

import ru.syskaev.edu.geekbrains.java.patterns.lesson3.units.Unit;

public class UnitObserver implements Observer{

    @Override
    public void update(Object subject, SomeInfo info) {
        Unit unit = (Unit)subject;
        System.out.println("Observer: " + unit.toString() + ": " + info.toString());
    }

}
