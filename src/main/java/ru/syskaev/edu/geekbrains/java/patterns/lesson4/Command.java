package ru.syskaev.edu.geekbrains.java.patterns.lesson4;

public interface Command {

    boolean doIt();

}
