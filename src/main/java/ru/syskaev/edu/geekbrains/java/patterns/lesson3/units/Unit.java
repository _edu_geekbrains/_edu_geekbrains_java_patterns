package ru.syskaev.edu.geekbrains.java.patterns.lesson3.units;

import ru.syskaev.edu.geekbrains.java.patterns.lesson5.UnitObserverConnector;

import java.awt.geom.Point2D;

public interface Unit {

    public UnitStats getUnitStats();
    public void setUnitStats();
    public void attack(Unit unit);
    public boolean goTo(Point2D targetPoint);
    public UnitObserverConnector getObserverConnector();
    //....

}
