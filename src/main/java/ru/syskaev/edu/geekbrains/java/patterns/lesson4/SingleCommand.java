package ru.syskaev.edu.geekbrains.java.patterns.lesson4;

import ru.syskaev.edu.geekbrains.java.patterns.lesson3.hero.Hero;

public class SingleCommand implements Command {

    public enum CommandType {ATTACK, RUN, GO_VILLAGE}

    protected Hero hero;
    protected CommandType commandType;


    public SingleCommand(Hero hero) {
        this.hero = hero;
    }

    @Override
    public boolean doIt() {
        System.out.println("Base Command");
        return true;
    }

}
