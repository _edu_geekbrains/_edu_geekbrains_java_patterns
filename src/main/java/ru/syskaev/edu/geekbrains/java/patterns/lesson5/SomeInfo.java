package ru.syskaev.edu.geekbrains.java.patterns.lesson5;

public class SomeInfo {

    private String message;
    private Object someData;

    public SomeInfo(String message, Object someData) {
        this.message = message;
        this.someData = someData;
    }

    public String getMessage() {
        return message;
    }

    public Object getSomeData() {
        return someData;
    }

    @Override
    public String toString() {
        return message + (someData == null ? "" : "\n" + someData.toString());
    }

}
