package ru.syskaev.edu.geekbrains.java.patterns.lesson3;

import ru.syskaev.edu.geekbrains.java.patterns.lesson3.hero.Hero;
import ru.syskaev.edu.geekbrains.java.patterns.lesson3.hero.HeroBuilder;
import ru.syskaev.edu.geekbrains.java.patterns.lesson3.units.Unit;
import ru.syskaev.edu.geekbrains.java.patterns.lesson3.units.UnitType;
import ru.syskaev.edu.geekbrains.java.patterns.lesson3.units.UnitsFactory;
import ru.syskaev.edu.geekbrains.java.patterns.lesson4.Command;
import ru.syskaev.edu.geekbrains.java.patterns.lesson4.ComplexCommand;
import ru.syskaev.edu.geekbrains.java.patterns.lesson4.SingleCommand;
import ru.syskaev.edu.geekbrains.java.patterns.lesson4.singlecommands.AttackCommand;
import ru.syskaev.edu.geekbrains.java.patterns.lesson4.singlecommands.MoveCommand;
import ru.syskaev.edu.geekbrains.java.patterns.lesson5.UnitObserver;

import java.util.ArrayList;
import java.util.List;

public class Game {

    private List<Unit> enemies = new ArrayList<>();
    private Hero hero;
    private UnitObserver unitObserver;

    private static Game instance = new Game();

    public static Game getInstance() { return instance; }

    private Game(/*...*/) {
        createEnemies();
        hero = new HeroBuilder()
                .name("Elena").sex('F').age(20).hitPoints(7).alignment("CG")
                .addToInventory("Potion").addToInventory("Magic staff")
                .build();
        //...
        startGameExample();
    }

    private void startGameExample() {
        unitObserver = new UnitObserver();
        for(Unit enemy : enemies)
            enemy.getObserverConnector().attachObserver(unitObserver);
        for(Unit enemy : enemies)
            enemy.attack(hero);
        //prepareTacticCommand().doIt();
    }

    private Command prepareTacticCommand() {
        MoveCommand runCommand = new MoveCommand(hero, SingleCommand.CommandType.RUN);
        AttackCommand attackCommand = new AttackCommand(hero);
        ComplexCommand regularCommand = new ComplexCommand();
        regularCommand.addCommand(runCommand);
        regularCommand.addCommand(attackCommand);
        MoveCommand goVillageCommand = new MoveCommand(hero, SingleCommand.CommandType.GO_VILLAGE);
        ComplexCommand tacticCommand = new ComplexCommand();
        tacticCommand.addCommand(regularCommand);
        tacticCommand.addCommand(regularCommand);
        tacticCommand.addCommand(regularCommand);
        tacticCommand.addCommand(goVillageCommand);
        return tacticCommand;
    }

    private void createEnemies() {
        enemies.add(UnitsFactory.createUnit(UnitType.WARRIOR));
        enemies.add(UnitsFactory.createUnit(UnitType.WARRIOR));
        enemies.add(UnitsFactory.createUnit(UnitType.WARRIOR));
        enemies.add(UnitsFactory.createUnit(UnitType.ROGUE));
        enemies.add(UnitsFactory.createUnit(UnitType.MAGE));
    }

    public void startGame() {
        //...
    }

}
