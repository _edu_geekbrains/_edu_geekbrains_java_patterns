package ru.syskaev.edu.geekbrains.java.patterns.lesson3.units;

import ru.syskaev.edu.geekbrains.java.patterns.lesson5.SomeInfo;
import ru.syskaev.edu.geekbrains.java.patterns.lesson5.UnitObserverConnector;

import java.awt.geom.Point2D;

public class Warrior implements Unit {

    private UnitStats unitStats = new UnitStats();
    private UnitObserverConnector observerConnector;

    public Warrior(/*...*/) {
        observerConnector = new UnitObserverConnector(this);
    }

    @Override
    public UnitStats getUnitStats() {
        //..
        return null;
    }

    @Override
    public void setUnitStats() {
        //..
    }

    @Override
    public void attack(Unit unit) {
        observerConnector.updateObserver(new SomeInfo("attack", null));
        //..
    }

    @Override
    public boolean goTo(Point2D targetPoint) {
        //..
        return false;
    }

    @Override
    public UnitObserverConnector getObserverConnector() {
        return observerConnector;
    }

}
