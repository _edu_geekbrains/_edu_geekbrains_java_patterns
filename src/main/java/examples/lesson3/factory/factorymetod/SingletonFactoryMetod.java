package examples.lesson3.factory.factorymetod;

/**
 * Created by i on 21.11.2017.
 */
public class SingletonFactoryMetod {
    private static final SingletonFactoryMetod ourInstance = new SingletonFactoryMetod();
    private static final FactoryMetod FACTORY_METOD = new FactoryMetodImpl();

    public static SingletonFactoryMetod getInstance() {
        return ourInstance;
    }

    private SingletonFactoryMetod() {
    }

    public FactoryMetod createFactoryMetod() {
        return FACTORY_METOD;
    }
}
