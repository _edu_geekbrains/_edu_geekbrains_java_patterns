package examples.lesson3.factory.factorymetod;

import java.text.NumberFormat;
import java.util.Calendar;

/**
 * Created by i on 21.11.2017.
 */
public class Main {
    public static void main(String[] args) {
        FactoryMetod factory = SingletonFactoryMetod.getInstance().createFactoryMetod();
        for (EnumProduct enumProduct : EnumProduct.values()) {
            Product  product = factory.createProduct(enumProduct);
            product.info();

            Calendar calendar = Calendar.getInstance();
            NumberFormat numberFormat = NumberFormat.getInstance();
        }
    }
}
