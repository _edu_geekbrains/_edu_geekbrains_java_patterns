package examples.lesson3.factory;

import java.util.Set;

public class Fabric {
    // определяем конфигурационные константы
    public static final String SUPPLIER_ONE = "Citilink";
    public static final String SUPPLIER_TWO = "Ulmart";
    public static final String SUPPLIER_3 = "Ozon";

    // создать объект, реализующий известный интерфейс на основе внешней информации
    public ExchangeFactory createFactory(String name) {
        switch (name) {
            case SUPPLIER_ONE:
                return new CitilinkExchangeFactory();
            case SUPPLIER_TWO:
                return null;
            case SUPPLIER_3:
                return new OzonExchangeFactory();
        }
        return null;
    }

    private static Fabric instance = new Fabric();

    public static Fabric getInstance() {
        return instance;
    }

    private Fabric() {
    }


}

