package examples.lesson3.factory;

public interface MarketingProvider {
    void claimSales();
    Money getBonus();

}
