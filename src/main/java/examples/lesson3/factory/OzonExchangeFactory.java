package examples.lesson3.factory;

public class OzonExchangeFactory implements ExchangeFactory {
    @Override
    public PriceProvider createPriceProvider() {
        return new OzonPriceProvider();
    }

    @Override
    public DocProvider createDocProvider() {
        return null;
    }

    @Override
    public MarketingProvider createMarketingProvider() {
        return null;
    }
}
