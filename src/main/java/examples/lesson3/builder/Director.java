package examples.lesson3.builder;

public class Director {
    private ComputerBuilder computerBuilder;

    public void setComputerBuilder(ComputerBuilder computerBuilder) {
        this.computerBuilder = computerBuilder;
    }

    public Computer getComputer() {
        return computerBuilder.getComputer();
    }

    public void constructComputer() {
        computerBuilder.createNewComputer()
                .buildSystemBlock()
                .buildDisplay()
                .buildManipulators();
    }

    public Computer constructAndGetComputer() {
        return computerBuilder.createNewComputer()
                .buildSystemBlock()
                .buildDisplay()
                .buildManipulators().getComputer();
    }
}
