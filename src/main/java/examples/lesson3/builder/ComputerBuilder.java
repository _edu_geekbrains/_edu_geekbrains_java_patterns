package examples.lesson3.builder;

public abstract class ComputerBuilder {
    protected Computer computer;

    public Computer getComputer() {
        return computer;
    }

    public ComputerBuilder createNewComputer() {
        computer = new Computer();
        return this;
    }

    public abstract ComputerBuilder buildSystemBlock();
    public abstract ComputerBuilder buildDisplay();
    public abstract ComputerBuilder buildManipulators();
}
