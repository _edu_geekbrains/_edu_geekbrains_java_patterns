package examples.lesson3.builder;

public class CheapComputerBuilder extends ComputerBuilder {
    public ComputerBuilder buildSystemBlock() {
        computer.setSystemBlock("Everest");
        return this;
    }

    public ComputerBuilder buildDisplay() {
        computer.setDisplay("CRT");
        return this;
    }

    public ComputerBuilder buildManipulators() {
        computer.setManipulators("mouse+keyboard");
        return this;
    }
}
