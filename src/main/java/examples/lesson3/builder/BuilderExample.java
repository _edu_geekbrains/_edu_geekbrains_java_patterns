package examples.lesson3.builder;

public class BuilderExample {
    public static void main(String[] args) {
        Director director = new Director();
        ComputerBuilder cheapComputerBuilder = new CheapComputerBuilder();

        director.setComputerBuilder(cheapComputerBuilder);
        director.constructComputer();

        Computer computer = director.getComputer();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("dgs").append(34).append(566).append("dfgdf");
}
}
