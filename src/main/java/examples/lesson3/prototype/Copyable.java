package examples.lesson3.prototype;

public interface Copyable {
    Copyable copy();
}
