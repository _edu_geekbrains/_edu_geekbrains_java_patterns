package examples.lesson3.prototype;

public class ComplicatedObject implements Copyable {
    private Type type;

    public enum Type {
        ONE, TWO
    }

    public ComplicatedObject copy() {
        ComplicatedObject clone =  new ComplicatedObject();
        clone.setType(type);
        return clone;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
